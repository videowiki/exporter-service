const storageService = require('@videowiki/vendors/storage');
const textToSpeechService = require('@videowiki/vendors/textToSpeech') 

module.exports = {
    storageService,
    textToSpeechService,
}